/**
 * @Author: Thiemo Leonhardt <thiemo>
 * @Date:   2017-08-22T16:28:48+02:00
 * @Email:  leonhardt@cs.rwth-aachen.de
 * @Last modified by:   thiemo
 * @Last modified time: 2018-02-06T08:21:20+01:00
 */

"use strict";

/**
 * This is the life cycle demo module.
 * It handles level progression and menu creation, as well as the game progress.
 * @namespace lc
 * @memberof MTLG
 */
var MTLG = (function(m) {
  var lifeCycle = (function() {
    var levels; //Saved levels: Object {entry : function(gameState), condition : function(gameState), lastPass : int}
    var menu; //Menu if present: function()
    var options; //manifest options
    var _currentLevel; // instance of the current level or menu
    var assets;

    /**
     * @function init -
     * Init function called in MTLG.init to set up the variables
     * @param {object} opt - the options currently available to the framework
     * @memberof! MTLG.lc
     */
    var init = function(opt) {
      levels = [];
      options = opt;
      assets = MTLG.assets;
    }
    //Register init function as module
    MTLG.addModule(init);


    /**
     * @function registerLevel - Description
     *
     * @param {function} entryCallback     The function that is called to create the new level
     * @param {function} conditionCallback The function that is called to evaluate if this level should be started next. Should return a number in [0,1] depending on the game state passed as a parameter.
     * @param {function} resizeCallback    The function that is called to resize the level
     * @memberof! MTLG.lc
     *
     */
    function registerLevel(entryCallback, conditionCallback, resizeCallback, assets) {
      var resizeCallback = resizeCallback || function() {
        console.log("No resizeCallback defined.");
      };
      if (!entryCallback || !conditionCallback) {
        console.log("MTLG.lc.registerLevel expects at least two arguments: The entry Callback and a condition Callback.");
        return;
      }
      levels.push({
        entry: entryCallback,
        condition: conditionCallback,
        lastPass: 0,
        resize: resizeCallback,
        assets
      });
    }


    /**
     * @function levelFinished - Call this function to get to the next level.
     * Tries to open menu if no levels are registered.
     * @param {object} gameState - the current gameState, that is passed to the conditionCallbacks to evaluate the next level.
     * @memberof! MTLG.lc
     */
    var levelFinished = function(gameState) {
      //Call the condition function for all levels to find how suitable they are
      //Also find the level with the highest lastPass value, this is the one we will go to.
      //If all values are 0 go to first level in levels.
      var max = 0;
      var maxI = 0;
      for (var i = 0; i < levels.length; i++) {
        try {
          levels[i].lastPass = (levels[i].condition)(gameState) || 0;
          if (levels[i].lastPass > max) {
            max = levels[i].lastPass;
            maxI = i;
          }
        } catch (err) {
          console.log("Error calling conditionCallback function: ");
          console.log(err);
          console.log(levels[i]);
        }
      }
      //Check if we found a level
      if (maxI >= 0 && maxI < levels.length) {
        try {
          var stage = MTLG.getStageContainer();
          var progressBar;
          assets.load(levels[maxI].assets, () => {
            // console.log("pause timer");
            // createjs.Ticker.setPaused(true);
            MTLG.clearStage();
            _resetScaling();
            levels[maxI].entry(gameState);
            // console.log("restart timer");
            // createjs.Ticker.setPaused(false);
            resizeStage();
          }, ({progress, totalProgress}) => {
            if(progressBar == null) {
              progressBar = new createjs.Text("MTLG", "400px Arial").set({x:.5*MTLG.getOptions().width,y:.5*MTLG.getOptions().height,textAlign:"center",textBaseline:"middle"});
              progressBar.set({color:"rgb(180,0,20)",shadow:new createjs.Shadow("",0,0,10)});
              stage.addChild(progressBar);
            }
            progressBar.shadow.color = `rgb(0,255,${Math.round(255*(1-totalProgress))})`;
            progressBar.alpha = .8 - .7*progress;
          });
          _currentLevel = levels[maxI]; // set the current level
        } catch (err) {
          console.log("Error calling entry callback: ");
          console.log(err);
          console.log(levels[maxI]);
          goToMenu(); //Something went wrong, go to menu
        }
      } else { //Go to menu
        goToMenu();
      }
    }


    /**
     * @function registerMenu - Registers a main menu
     *
     * @param {function} entryCallback  The function that creates the menu
     * @param {function} resizeCallback The function that resizes the menu
     *
     * @memberof! MTLG.lc
     */
    function registerMenu(entryCallback, resizeMainMenu, assets) {
      var resize = resizeMainMenu || function() {
        console.log("No resizeCallback defined.");
      };
      if (!entryCallback) {
        console.log("Registration of a menu requires the entry callback as parameter!");
        return;
      } else {
        menu = entryCallback;
        menu.resize = resize;
        menu.assets = assets;
      }
    }

    /**
     * @function getLevels - returns an array of the entry functions of all levels registered. Does not include the main menu.
     * @return {array} - all levels but not the main menu
     * @memberof! MTLG.lc
     */
    var getLevels = function() {
      return levels;
    }
    /**
     * @function getMenu - returns the entry function of the main menu.
     * @return {function} The entry function of the main menu.
     * @memberof! MTLG.lc
     */
    var getMenu = function() {
      return menu;
    }
    /**
     * @function hasMenu - returns true if a menu was registered.
     * @return {bool} - true is menu exists, false otherwise.
     * @memberof! MTLG.lc
     */
    var hasMenu = function() {
      return (menu != undefined);
    }

    /**
     * @function goToMenu - calls the main menu entry function.
     * @memberof! MTLG.lc
     */
    var goToMenu = function() {
      var stage = MTLG.getStageContainer();
      var progressBar;
      try {
        _currentLevel = menu;
        assets.load(menu.assets, () => {
          MTLG.clearStage();
          _resetScaling();
          console.log("assets loaded!");
          menu();
          resizeStage();
        }, ({progress, totalProgress}) => {
          if(progressBar == null) {
            progressBar = new createjs.Text("MTLG", "400px Arial").set({x:.5*MTLG.getOptions().width,y:.5*MTLG.getOptions().height,textAlign:"center",textBaseline:"middle"});
            progressBar.set({color:"rgb(180,0,20)",shadow:new createjs.Shadow("",0,0,10)});
            stage.addChild(progressBar);
          }
          progressBar.shadow.color = `rgb(0,255,${Math.round(255*(1-totalProgress))})`;
          progressBar.alpha = .8 - .7*progress;
        });
      } catch (err) {
        console.log("Unable to create menu");
        console.log(err);
        console.log(menu);
      }
    }

    /**
     * @function startGame -
     * Starts the game.
     * If the game has its own main menu go there.
     * Else start the game with the login screen.
     * @memberof! MTLG.lc
     */
    var startGame = function() {
      MTLG.clearStage();
      _resetScaling();
      resizeStage();
      if (menu) {
        goToMenu();
      } else {
        goToLogin();
      }
    }

    /**
     * @function goToLogin - use this function to trigger the login process of the MTLG framework.
     * New users will be added to the framework and can be retrieved using the corresponding MTLG function.
     * See the wiki or API for more information on the login process.
     * @memberof! MTLG.lc
     */
    var goToLogin = function() {
      MTLG.clearStage();
      _resetScaling();
      MTLG.erstelleLoginScreen(MTLG.getStage(), options.loginMode); //3rd param: Methode. Standard: Messe
      resizeStage();
    }

    var _scaleOld = 1;

    function _resetScaling() {
      _scaleOld = 1;
    }

    /**
     * @function resizeStage -
     * resizeStage - Triggers a stage resize
     *
     * @return {number} the new scale factor
     * @memberof! MTLG.lc
     */
    function resizeStage() {

      let i, l;
      let children = MTLG.getStage().children;
      let scaleX = window.innerWidth / MTLG.getOptions().width;
      let scaleY = window.innerHeight / MTLG.getOptions().height;
      let scaleFactor = Math.min(scaleX, scaleY);
      let bgStage = MTLG.getBackgroundStage();

      for (i = 0, l = children.length; i < l; i++) {
        // children smaller
        children[i].scaleX = children[i].scaleX / _scaleOld * scaleFactor;
        children[i].scaleY = children[i].scaleY / _scaleOld * scaleFactor;
        // put them on the new position
        children[i].x = children[i].x / _scaleOld * scaleFactor;
        children[i].y = children[i].y / _scaleOld * scaleFactor;
      }

      // scale Background
      bgStage.scaleX = scaleFactor;
      bgStage.scaleY = scaleFactor;
      bgStage.update();

      // safe old values
      _scaleOld = scaleFactor;

      // the level could resize itself
      if (_currentLevel) _currentLevel.resize();
      return scaleFactor;
    }

    return {
      resizeStage: resizeStage,
      registerLevel: registerLevel,
      registerMenu: registerMenu,
      levelFinished: levelFinished,
      getLevels: getLevels,
      hasMenu: hasMenu,
      goToMenu: goToMenu,
      startGame: startGame,
      goToLogin: goToLogin,
      init: init,
    }
  })();
  m.lc = lifeCycle;
  return m;
})(MTLG);
